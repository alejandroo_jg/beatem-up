using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueFuerte : MonoBehaviour
{
    [SerializeField] private float velocidad;

    private void Update()
    {
        transform.Translate(Vector2.right * velocidad * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("HurtBoxEnemy") && collision.gameObject.CompareTag("Enemigo"))
        {
            Destroy(collision.gameObject);
            GameManager gameManager = FindObjectOfType<GameManager>();

            if (gameManager != null)
            {
                gameManager.EnemigoMuerto();
            }
            else
            {
                Debug.LogWarning("GameManager no encontrado en la jerarqu�a.");
            }
        }
    }
}
