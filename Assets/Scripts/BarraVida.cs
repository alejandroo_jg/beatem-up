using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    public GameManager gameManager;
    public Image barraVida;
    public Color fullColor = Color.green;
    public Color halfColor = Color.yellow;
    public Color lowColor = Color.red;
    public float vida;

    private void Awake()
    {
        ActualizarBarra();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void DisminuirVida(float cantidad)
    {
        vida -= cantidad;
        vida = Mathf.Max(vida, 0f);
        ActualizarBarra();
        if (vida <= 0f)
        {
            gameManager.GameOver();
        }
    }

    void ActualizarBarra()
    {
        if (vida > 60f)
        {
            barraVida.color = fullColor;
        }
        else if (vida > 25f)
        {
            barraVida.color = halfColor;
        }
        else
        {
            barraVida.color = lowColor;
        }

        float newScaleX = vida / 100f;
        barraVida.rectTransform.localScale = new Vector3(newScaleX, 1f, 1f);
    }


}
