using UnityEngine.SceneManagement;
using UnityEngine;

public class CambioEscena : MonoBehaviour
{
    public string escena = "Juego";

    public void Cambio()
    {
        SceneManager.LoadScene(escena);
    }
}

