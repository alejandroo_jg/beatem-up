using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public float minX = -10f;
    public float maxX = 10f;
    public float minY = -5f;
    public float maxY = 5f;

    void Start()
    {

    }

    void Update()
    {
        // Obtiene la posici�n del jugador
        float x = player.transform.position.x;
        float y = player.transform.position.y;

        // Aplica los l�mites a la posici�n de la c�mara
        x = Mathf.Clamp(x, minX, maxX);
        y = Mathf.Clamp(y, minY, maxY);

        // Asigna la nueva posici�n a la c�mara
        transform.position = new Vector3(x, y, transform.position.z);
    }
}