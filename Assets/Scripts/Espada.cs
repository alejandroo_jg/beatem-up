using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espada : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("HurtBox") && collision.gameObject.CompareTag("Player"))
        {
            BarraVida barraVida = collision.gameObject.GetComponent<BarraVida>();
            if (barraVida != null)
            {
                barraVida.DisminuirVida(10f);
            }
        }
    }
}
