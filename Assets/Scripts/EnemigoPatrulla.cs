using UnityEngine;

public class EnemigoPatrulla : MonoBehaviour
{
    public Transform player;
    [SerializeField] private Transform controladorPatrulla;
    [SerializeField] private Transform controladorDisparo;
    [SerializeField] private float radioPatrulla;
    [SerializeField] private GameObject Bala;
    public float radioDisparo;
    public float velocidad = 2f;
    public float velocidadPersecucion = 4f;

    public Vector3 puntoA;
    public Vector3 puntoB;
    private enum EnemyState { Patrulla, Perseguir, Disparo, Esperar }
    private EnemyState currentState = EnemyState.Patrulla;

    private float tiempoEspera = 3f;
    private float tiempoEsperaActual = 0f;
    private float tiempoEntreDisparos = 2f;
    private float tiempoSiguienteDisparo = 0f;

    private Vector3 posicionInicial;

    private void Start()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        switch (currentState)
        {
            case EnemyState.Patrulla:
                transform.position = Vector3.Lerp(puntoA, puntoB, Mathf.PingPong(Time.time * velocidad, 1));
                Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorPatrulla.position, radioPatrulla);
                foreach (Collider2D collider in objetos)
                {
                    if (collider.CompareTag("Player"))
                    {
                        posicionInicial = transform.position;
                        currentState = EnemyState.Perseguir;
                    }
                }
                break;

            case EnemyState.Perseguir:
                Vector3 direccion = (player.position - transform.position).normalized;
                transform.Translate(direccion * velocidadPersecucion * Time.deltaTime);
                float distanciaAlJugador = Vector3.Distance(transform.position, player.position);

                if (distanciaAlJugador <= radioDisparo)
                {
                    if (Time.time >= tiempoSiguienteDisparo)
                    {
                        currentState = EnemyState.Disparo;
                    }
                }
                break;

            case EnemyState.Disparo:
                if (Time.time >= tiempoSiguienteDisparo)
                {
                    Instantiate(Bala, controladorDisparo.position, controladorDisparo.rotation);
                    tiempoSiguienteDisparo = Time.time + tiempoEntreDisparos;
                }

                float radioAlJugador = Vector3.Distance(transform.position, player.position);
                if (radioAlJugador > radioDisparo)
                {
                    currentState = EnemyState.Esperar;
                }
                break;

            case EnemyState.Esperar:
                tiempoEsperaActual += Time.deltaTime;
                Collider2D[] objetivo = Physics2D.OverlapCircleAll(controladorDisparo.position, radioDisparo);
                foreach (Collider2D collider in objetivo)
                {
                    if (collider.CompareTag("Player"))
                    {
                        currentState = EnemyState.Disparo;
                    }
                }
                if (tiempoEsperaActual >= tiempoEspera)
                {
                    transform.position = posicionInicial;
                    tiempoEsperaActual = 0f;
                    currentState = EnemyState.Patrulla;
                }
                break;
        }
    }
}
