using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float velocidad;
    private void Update()
    {
        transform.Translate(Vector2.right * velocidad * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BarraVida barraVida = collision.gameObject.GetComponent<BarraVida>();
            if (barraVida != null)
            {
                barraVida.DisminuirVida(15f);
            }
        }

        Destroy(gameObject);
    }
}
