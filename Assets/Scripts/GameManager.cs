using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject enemigoEspada;
    public GameObject enemigoEscopeta1;
    public GameObject enemigoEscopeta2;
    public TextMeshProUGUI roundText;
    public TextMeshProUGUI textoronda;

    private int ronda = 1;
    public int enemigosVivos = 0;

    public Transform[] puntosSpawn;
    public Transform puntoA;
    public Transform puntoB;
    public int maxEnemigosPorPunto = 2;

    private List<Transform> puntosOcupados = new List<Transform>();

    void Start()
    {
        StartCoroutine(ComenzarRondaConTexto());
    }
    void Update()
    {
        textoronda.text = "Round " + ronda;
    }

    IEnumerator ComenzarRondaConTexto()
    {
        roundText.text = "Round " + ronda;
        yield return new WaitForSeconds(2.5f);
        roundText.text = "";

        ComenzarRonda();
    }

    void ComenzarRonda()
    {
        switch (ronda)
        {
            case 1:
                GenerarEnemigosEspada(5);
                break;
            case 2:
                GenerarEnemigoEscopeta1();
                GenerarEnemigoEscopeta2();
                break;
            case 3:
                GenerarEnemigosEspada(7);
                GenerarEnemigoEscopeta1();
                GenerarEnemigoEscopeta2();
                break;
        }
    }

    void GenerarEnemigosEspada(int cantidad)
    {
        for (int i = 0; i < cantidad; i++)
        {
            Transform puntoSpawn = ObtenerPuntoSpawnLibre();
            if (puntoSpawn != null)
            {
                Vector3 posicionSpawn = puntoSpawn.position;
                Instantiate(enemigoEspada, posicionSpawn, Quaternion.identity);
                puntosOcupados.Add(puntoSpawn);
                enemigosVivos++;
            }
        }
    }

    void GenerarEnemigoEscopeta1()
    {
        Instantiate(enemigoEscopeta1, puntoA.position, puntoA.rotation);
        enemigosVivos++;
    }
    void GenerarEnemigoEscopeta2()
    {
        Instantiate(enemigoEscopeta2, puntoB.position, puntoB.rotation);
        enemigosVivos++;
    }

    Transform ObtenerPuntoSpawnLibre()
    {
        List<Transform> puntosLibres = new List<Transform>();

        foreach (Transform punto in puntosSpawn)
        {
            if (!puntosOcupados.Contains(punto))
            {
                puntosLibres.Add(punto);
            }
        }

        if (puntosLibres.Count > 0)
        {
            return puntosLibres[Random.Range(0, puntosLibres.Count)];
        }

        return null;
    }

    public void EnemigoMuerto()
    {
        enemigosVivos--;

        if (enemigosVivos == 0)
        {
            puntosOcupados.Clear();
            ronda++;

            if (ronda <= 3)
            {
                StartCoroutine(ComenzarRondaConTexto());
            }
            else
            {

            }
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
