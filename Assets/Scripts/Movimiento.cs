using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public GameManager gameManager;
    private enum EstadosJugador { NINGUNO, QUIETO, CAMINAR, ATACARFLOJO, ATACARFUERTE };
    private EstadosJugador estadoActual;

    private Rigidbody2D rb2D;
    private Animator animador;

    [Header("Movimiento")]
    private Vector2 movimiento = Vector2.zero;
    [SerializeField] private float velocidadDeMovimiento;

    [Range(0, 0.3f)][SerializeField] private float suavizadoDeMovimiento;
    private Vector3 velocidad = Vector3.zero;
    private EnMovimiento entradasMovimiento;
    private bool mirandoDerecha = true;

    [Header("Ataque")]
    [SerializeField] private GameObject AtaqueFlojo;
    [SerializeField] private GameObject AtaqueFuerte;
    [SerializeField] private Transform controladorAtaque;
    [SerializeField] private Transform controladorAtaqueFuerte;
    [SerializeField] private float tiempoDesaparicionAtaque;
    [SerializeField] private float radioAtaque;
    [SerializeField] private float radioAtaqueFuerte;
    [SerializeField] private float dañoAtaque;
    public bool sePuedeMover = true;
    [SerializeField] private LayerMask enemy;

    private bool ataqueEnProgreso = false;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animador = GetComponent<Animator>();
        entradasMovimiento = new EnMovimiento();
        entradasMovimiento.Enable();
        CambiarEstado(EstadosJugador.QUIETO);
    }

    private void CambiarEstado(EstadosJugador nuevoEstado)
    {
        if (nuevoEstado == estadoActual)
            return;

        SalirEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(EstadosJugador nuevoEstado)
    {
        estadoActual = nuevoEstado;

        switch (estadoActual)
        {
            case EstadosJugador.QUIETO:
                rb2D.velocity = Vector2.zero;
                animador.Play("Quieto");
                break;

            case EstadosJugador.CAMINAR:
                animador.Play("Caminar");
                break;

            case EstadosJugador.ATACARFLOJO:

                break;

            default:
                break;
        }
    }

    private void SalirEstado()
    {

    }

    private void Update()
    {
        movimiento = entradasMovimiento.Movimiento.Movimiento.ReadValue<Vector2>() * velocidadDeMovimiento;

        switch (estadoActual)
        {
            case EstadosJugador.QUIETO:
                if (movimiento.x != 0 || movimiento.y != 0)
                    CambiarEstado(EstadosJugador.CAMINAR);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    CambiarEstado(EstadosJugador.ATACARFLOJO);
                }
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    CambiarEstado(EstadosJugador.ATACARFUERTE);
                }
                break;

            case EstadosJugador.CAMINAR:
                if (sePuedeMover)
                {
                    Mover(movimiento * Time.fixedDeltaTime);
                }
                if (movimiento == Vector2.zero)
                    CambiarEstado(EstadosJugador.QUIETO);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    CambiarEstado(EstadosJugador.ATACARFLOJO);
                }
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    CambiarEstado(EstadosJugador.ATACARFUERTE);
                }
                break;

            case EstadosJugador.ATACARFLOJO:
                if (!ataqueEnProgreso)
                {
                    if (sePuedeMover)
                    {
                        ataqueEnProgreso = true;
                        AtacarFlojo();
                        StartCoroutine(LanzarAtaqueFlojo());
                    }
                }
                if (movimiento == Vector2.zero)
                    CambiarEstado(EstadosJugador.QUIETO);
                if (movimiento.x != 0 || movimiento.y != 0)
                    CambiarEstado(EstadosJugador.CAMINAR);
                break;

            case EstadosJugador.ATACARFUERTE:
                if (!ataqueEnProgreso)
                {
                    if (sePuedeMover)
                    {
                        ataqueEnProgreso = true;
                        StartCoroutine(LanzarAtaqueFuerte());
                    }
                }
                if (movimiento == Vector2.zero)
                    CambiarEstado(EstadosJugador.QUIETO);
                if (movimiento.x != 0 || movimiento.y != 0)
                    CambiarEstado(EstadosJugador.CAMINAR);
                break;

            default:
                break;
        }
    }

    IEnumerator LanzarAtaqueFlojo()
    {
        sePuedeMover = false;
        GameObject ataqueFlojoInstance = Instantiate(AtaqueFlojo, controladorAtaque.position, controladorAtaque.rotation);
        if (!mirandoDerecha)
        {
            Vector3 escala = ataqueFlojoInstance.transform.localScale;
            escala.x *= -1;
            ataqueFlojoInstance.transform.localScale = escala;
        }
        ataqueFlojoInstance.transform.SetParent(controladorAtaque);
        yield return new WaitForSeconds(tiempoDesaparicionAtaque);
        Destroy(ataqueFlojoInstance);
        ataqueEnProgreso = false;
        sePuedeMover = true;
    }

    IEnumerator LanzarAtaqueFuerte()
    {
        sePuedeMover = false;
        GameObject ataqueFuerteInstance = Instantiate(AtaqueFuerte, controladorAtaqueFuerte.position, controladorAtaqueFuerte.rotation);
        if (!mirandoDerecha)
        {
            Vector3 escala = ataqueFuerteInstance.transform.localScale;
            escala.x *= -1;
            ataqueFuerteInstance.transform.localScale = escala;
        }
        ataqueFuerteInstance.transform.SetParent(controladorAtaque);
        yield return new WaitForSeconds(0.6F);
        Destroy(ataqueFuerteInstance);
        ataqueEnProgreso = false;
        sePuedeMover = true;
    }

    private void Mover(Vector2 mover)
    {
        Vector3 velocidadObjetivo = mover;
        rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoDeMovimiento);

        if (mover.x > 0 && !mirandoDerecha)
        {
            Girar();
        }
        else if (mover.x < 0 && mirandoDerecha)
        {
            Girar();
        }
    }

    private void Girar()
    {
        mirandoDerecha = !mirandoDerecha;
        Vector3 escala = transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }
    private void AtacarFlojo()
    {
        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorAtaque.position, radioAtaque, enemy);
        foreach (Collider2D collider in objetos)
        {
            if (collider.CompareTag("Enemigo"))
            {
                Destroy(collider.gameObject);
                gameManager.EnemigoMuerto();
            }
        }
    }
}


