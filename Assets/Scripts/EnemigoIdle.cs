using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoIdle : MonoBehaviour
{
    private enum EstadosEnemigo { NINGUNO, QUIETO, PERSEGUIR, ATACAR };
    private EstadosEnemigo estadoActual;

    private Rigidbody2D rb2D;
    private Animator animador;

    [SerializeField] private Transform controladorPersecución;
    [SerializeField] private Transform controladorAtaque;
    [SerializeField] private Transform controladorEspada;
    [SerializeField] private Transform controladorEspada2;
    [SerializeField] private float radioAtaque;
    [SerializeField] private float radioPersecucion;
    [SerializeField] private float dañoAtaque;
    public float velocidad = 5f;
    public Transform jugador;

    public GameObject Espada;
    private bool persiguiendo = false;
    private bool ataqueEnProgreso = false;
    private float tiempoEntreAtaques = 1.5f;
    private float tiempoSiguienteAtaque = 0f;


    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animador = GetComponent<Animator>();
        CambiarEstado(EstadosEnemigo.QUIETO);
        if (jugador == null)
        {
            jugador = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void CambiarEstado(EstadosEnemigo nuevoEstado)
    {
        if (nuevoEstado == estadoActual)
            return;

        SalirEstado();
        IniciarEstado(nuevoEstado);
    }

    private void IniciarEstado(EstadosEnemigo nuevoEstado)
    {
        estadoActual = nuevoEstado;

        switch (estadoActual)
        {
            case EstadosEnemigo.QUIETO:
                rb2D.velocity = Vector2.zero;
                animador.Play("QuietoEnemigo");
                break;

            case EstadosEnemigo.PERSEGUIR:
                animador.Play("Perseguir");
                break;

            case EstadosEnemigo.ATACAR:
                animador.Play("Atacar");
                break;

            default:
                break;
        }
    }

    private void SalirEstado()
    {

    }

    private void Update()
    {

        switch (estadoActual)
        {
            case EstadosEnemigo.QUIETO:
                Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorPersecución.position, radioPersecucion);
                foreach (Collider2D collider in objetos)
                {
                    if (collider.CompareTag("Player"))
                    {
                        CambiarEstado(EstadosEnemigo.PERSEGUIR);

                    }
                }

                break;

            case EstadosEnemigo.PERSEGUIR:
                velocidad = 4;
                float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);
                if (distanciaAlJugador <= radioPersecucion)
                {
                    persiguiendo = true;
                }
                else
                {
                    persiguiendo = false;
                }
                Vector3 direccion = (jugador.position - transform.position).normalized;
                transform.position += direccion * velocidad * Time.deltaTime;

                if (!persiguiendo)
                    CambiarEstado(EstadosEnemigo.QUIETO);

                Collider2D[] colliders = Physics2D.OverlapCircleAll(controladorAtaque.position, radioAtaque);
                foreach (Collider2D collider in colliders)
                {
                    if (collider.CompareTag("Player"))
                    {
                        CambiarEstado(EstadosEnemigo.ATACAR);
                    }
                }
                break;

            case EstadosEnemigo.ATACAR:
                float radioAlJugador = Vector3.Distance(transform.position, jugador.position);
                if (radioAlJugador <= radioAtaque)
                {
                    ataqueEnProgreso = true;
                    if (Time.time >= tiempoSiguienteAtaque)
                    {
                        StartCoroutine(LanzarAtaque());
                        StartCoroutine(LanzarAtaque2());
                        tiempoSiguienteAtaque = Time.time + tiempoEntreAtaques;
                    }
                }
                else
                {
                    ataqueEnProgreso = false;
                }
                if (!ataqueEnProgreso)
                    CambiarEstado(EstadosEnemigo.PERSEGUIR);
                break;

            default:
                break;
        }
    }

    IEnumerator LanzarAtaque()
    {
        GameObject espadaInstance = Instantiate(Espada, controladorEspada.position, controladorEspada.rotation);
        espadaInstance.transform.SetParent(controladorEspada);
        yield return new WaitForSeconds(0.5f);
        Destroy(espadaInstance);
    }

    IEnumerator LanzarAtaque2()
    {
        GameObject espadaInstance = Instantiate(Espada, controladorEspada2.position, controladorEspada2.rotation);
        Vector3 escala = espadaInstance.transform.localScale;
        escala.x *= -1;
        espadaInstance.transform.localScale = escala;
        espadaInstance.transform.SetParent(controladorEspada2);
        yield return new WaitForSeconds(0.5f);
        Destroy(espadaInstance);
    }
}

